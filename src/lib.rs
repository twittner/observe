extern crate nohash_hasher;
extern crate parking_lot;

#[cfg(feature = "parallel")]
extern crate rayon;

use nohash_hasher::IntMap;
use parking_lot::RwLock;
use std::sync::{atomic::{AtomicBool, Ordering}, Arc, Weak};

/// An observable event type which, when triggered carries a payload `T` and
/// whose callbacks return a value of type `R`.
pub struct Event<T, R = ()> {
    inner: Arc<RwLock<Inner<T, R>>>
}

// Manual impl to not require `T: Clone`, `R: Clone`.
impl<T, R> Clone for Event<T, R> {
    fn clone(&self) -> Self {
        Event { inner: self.inner.clone() }
    }
}

struct Inner<T, R> {
    index: usize,
    observers: IntMap<usize, Box<dyn Fn(&T) -> R + Send + Sync>>
}

impl<T, R> Event<T, R> {
    /// Create a new observable event.
    pub fn new() -> Self {
        let inner = Inner {
            index: 0,
            observers: IntMap::default()
        };
        Event {
            inner: Arc::new(RwLock::new(inner))
        }
    }

    /// Observe this event.
    ///
    /// A `Link` is returned which can be used to cancel the observation.
    pub fn observe<F>(&self, f: F) -> Link<T, R>
    where
        F: Fn(&T) -> R + Send + Sync + 'static
    {
        let mut inner = self.inner.write();
        let i = inner.index;
        inner.observers.insert(i, Box::new(f));
        inner.index += 1;
        Link {
            index: i,
            events: Arc::downgrade(&self.inner),
            active: Arc::new(AtomicBool::new(true))
        }
    }

    /// Triggers an event with payload `T` which will reach observers.
    ///
    /// The return value of the last observer (if any) will be returned.
    pub fn fire(&self, val: T) -> Option<R> {
        self.inner.read()
            .observers
            .values()
            .fold(None, |_, f| Some(f(&val)))
    }
}

#[cfg(feature = "parallel")]
impl<T: Sync, R> Event<T, R> {
    /// Like `fire` but notifies observes in parallel and unordered.
    pub fn fire_par(&self, x: T) {
        use rayon::prelude::*;
        self.inner.read()
            .observers
            .par_iter()
            .for_each(|(_, f)| { f(&x); })
    }
}

/// A link to an observed event.
pub struct Link<T, R> {
    index: usize,
    events: Weak<RwLock<Inner<T, R>>>,
    active: Arc<AtomicBool>
}

// Manual impl to not require `T: Clone`, `R: Clone`.
impl<T, R> Clone for Link<T, R> {
    fn clone(&self) -> Self {
        Link {
            index: self.index,
            events: self.events.clone(),
            active: self.active.clone()
        }
    }
}

impl<T, R> Link<T, R> {
    /// Remove this link from the event, cancelling the observation.
    ///
    /// Implicit cancellation happens on drop.
    pub fn cancel(&self) {
        if !self.active.swap(false, Ordering::Relaxed) {
            return ()
        }
        if let Some(s) = self.events.upgrade() {
            s.write().observers.remove(&self.index);
        }
    }
}

impl<T, R> Drop for Link<T, R> {
    fn drop(&mut self) {
        self.cancel()
    }
}

#[cfg(test)]
mod tests {
    use std::sync::atomic::{AtomicUsize, Ordering};
    use super::*;

    #[test]
    fn simple() {
        let e = Event::<u8>::new();

        let k1 = Arc::new(AtomicUsize::new(0));
        let k2 = k1.clone();
        let k3 = k2.clone();

        let l1 = e.observe(move |n| {
            k1.fetch_add(*n as usize, Ordering::SeqCst);
        });
        assert_eq!(1, e.inner.read().index);
        assert_eq!(1, e.inner.read().observers.keys().count());
        assert_eq!(0, l1.index);
        assert!(l1.active.load(Ordering::Relaxed));

        let l2 = e.observe(move |n| {
            k2.fetch_add(*n as usize, Ordering::SeqCst);
        });
        assert_eq!(2, e.inner.read().index);
        assert_eq!(2, e.inner.read().observers.keys().count());
        assert_eq!(1, l2.index);
        assert!(l2.active.load(Ordering::Relaxed));

        e.fire(3);
        assert_eq!(6, k3.load(Ordering::SeqCst))
    }

    #[test]
    fn link() {
        let e = Event::<()>::new();
        let l = e.observe(|()| ());
        assert_eq!(1, e.inner.read().index);
        assert_eq!(1, e.inner.read().observers.keys().count());
        assert_eq!(0, l.index);
        assert!(l.active.load(Ordering::Relaxed));

        let x = l.clone();
        assert_eq!(0, x.index);
        assert!(x.active.load(Ordering::Relaxed));

        l.cancel();
        assert!(!l.active.load(Ordering::Relaxed));
        assert!(!x.active.load(Ordering::Relaxed));
        assert_eq!(0, e.inner.read().observers.keys().count());
    }

    #[test]
    fn drop() {
        let e = Event::<()>::new();
        {
            let l = e.observe(|()| ());
            assert_eq!(1, e.inner.read().index);
            assert_eq!(1, e.inner.read().observers.keys().count());
            assert_eq!(0, l.index);
            assert!(l.active.load(Ordering::Relaxed));
        }
        assert_eq!(1, e.inner.read().index);
        assert_eq!(0, e.inner.read().observers.keys().count());
    }
}
